google.load("feeds", "1");

    function initialize() {
      var feed = new google.feeds.Feed("http://wedspiration.com/feed/");
	  feed.setResultFormat(google.feeds.Feed.MIXED_FORMAT);
      feed.load(function(result) {
        if (!result.error) {
          var container = document.getElementById("feed");
          for (var i = 0; i < 1; i++) {
            var entry = result.feed.entries[i];
			var entryImageUrl = entry.xmlNode.getElementsByTagName("enclosure")[0].getAttribute("url");
			
			var pthumb = document.createElement("p");
			var p = document.createElement("p");
			var img = document.createElement("img");
			var h3 = document.createElement("h3");
			var a = document.createElement("a");
			var morea = document.createElement("a");
			var moretext = 'Lees meer';
            var div = document.createElement("div");
			
			img.setAttribute("src",entryImageUrl);
			img.setAttribute("alt",entry.title);
			pthumb.setAttribute("class",'thumb');
			pthumb.appendChild(img);
			
			// title
			a.appendChild(document.createTextNode(entry.title));
			a.setAttribute("href",entry.link);
			a.setAttribute("target","_blank");
            h3.appendChild(a);			
			div.appendChild(h3);
			
			// description
			p.appendChild(document.createTextNode(entry.contentSnippet));
			div.appendChild(p);
			
			// more link
			morea.appendChild(document.createTextNode(moretext));
			morea.setAttribute("href",entry.link);
			morea.setAttribute("target","_blank");
			div.appendChild(morea);
			
			container.appendChild(pthumb);
            container.appendChild(div);
          }
        }
      });
    }
    google.setOnLoadCallback(initialize);