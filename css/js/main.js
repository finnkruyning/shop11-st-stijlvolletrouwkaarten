function placeHolder(element){
	var standard_message = $(element).val();
	$(element).focus(
		function() {
			if ($(this).val() == standard_message)
				$(this).val("");
		}
	);
	$(element).blur(
		function() {
			if ($(this).val() == "")
				$(this).val(standard_message);
		}
	);
}

$.fn.randomize = function(selector){
	var $elems = selector ? $(this).find(selector) : $(this).children(),
		$parents = $elems.parent();

	$parents.each(function(){
		$(this).children(selector).sort(function(){
			return Math.round(Math.random()) - 0.5;
			// }). remove().appendTo(this); // 2014-05-24: Removed `random` but leaving for reference. See notes under 'ANOTHER EDIT'
		}).detach().appendTo(this);
	});

	return this;
};

jQuery(document).ready(function($){
	var $fotoInner = $('.foto-inner');

	$fotoInner.each(function(index, el) {
		var $fotoPrice = $(el).find('.price');

		if(!$fotoPrice.length) {
			$(el).find('.addr').css({
				'width': 'auto',
				'float': 'none'
			});
		}
	});

	if (jQuery('.foto-list').length) {
		jQuery('.foto-list').randomize('li');
	}

	jQuery('.lightbox').click(function(event){
		event.preventDefault();
		jQuery('body').append('<div id="bg-lightbox"></div>').show('slow');
		jQuery('body').append('<div id="lightbox"><script>jQuery(document).ready(function(){jQuery("#bg-lightbox").click(function(){jQuery(this).hide("slow");jQuery(this).remove();jQuery("#lightbox").hide("slow");jQuery("#lightbox").remove();});jQuery("#lightbox .close").click(function(){jQuery("#bg-lightbox").hide("slow");jQuery("#bg-lightbox").remove();jQuery("#lightbox").hide("slow");jQuery("#lightbox").remove();});});</script><span class="close">Close</span><iframe src="/css/js/calculator/calculator.html"></iframe></div>').show('slow');
	});

	jQuery('<p class="leadtext">Bestel een GRATIS proefdruk met de code <span>stijlvol</span></p>').prependTo("#pagecontent");

	jQuery("#body_home-test #footer #footer-inner").remove();

	jQuery(".card-list li:last-child").addClass('last');
	jQuery(".block-list li:odd").addClass('right');

	var tophead = jQuery("#headermenu").html() + jQuery(".fixed").html();
	jQuery('#wrapper').before("<div class='tophead'><div class='topheadinner'>" + tophead + "</div></div>");

	var fnav = jQuery(".fnav-block").html();
	jQuery('body').append(fnav);


	jQuery('#headerlogin span').replaceWith('<span>Login</span>');
	jQuery('#headerlogout span').replaceWith('<span>Logout</span>');

	$(".search-form input[type='text']").each(function(index, element) {
		  placeHolder($(this));
	});

	jQuery("#body_account #pagecontent p .button:eq(2)").remove();
	jQuery("#body_account #pagecontent p .button:eq(2)").prev().remove();
	jQuery("#body_account #pagecontent p .button:eq(2)").prev().remove();
	jQuery("#body_account #pagecontent p .button:eq(2)").remove();
	jQuery("#body_account #pagecontent p .button:eq(2)").prev().remove();
	jQuery("#body_account #pagecontent p .button:eq(2)").prev().remove();

	jQuery('div#menu ul').superfish({
		delay:       300,                            // one second delay on mouseout
		animation:   {'marginLeft':'0px',opacity:'show',height:'show'},  // fade-in and slide-down animation
		speed:       'fast',                          // faster animation speed
		autoArrows:  true,                           // disable generation of arrow mark-up
		onBeforeShow:      function(){ this.css('marginLeft','20px'); },
		dropShadows: false                            // disable drop shadows
	});

	jQuery('#card_previews a img').hover(function(){
		jQuery(this).animate({opacity: 0.7},500);
	},function(){
		jQuery(this).animate({opacity: 1},500);
	});

	jQuery("#navInfo").hover(function(){
		jQuery('ul',this).stop(true, true).delay(200).slideDown(200);
	}, function() {
		jQuery('ul',this).stop(true, true).slideUp(200);
	});

	jQuery('#headerhelp').remove();

	jQuery("#navHulp").hover(function(){
		jQuery('ul',this).stop(true, true).delay(200).slideDown(200);
	}, function() {
		jQuery('ul',this).stop(true, true).slideUp(200);
	});

	jQuery(".drop").hover(function(){
		jQuery('ul',this).stop(true, true).delay(200).slideDown(200);
	}, function() {
		jQuery('ul',this).stop(true, true).slideUp(200);
	});

	$('.blockquote').innerfade({
		speed: 1500,
		timeout: 10000,
		type: 'random_start',
		containerheight: '120px'
	});

	$('.feedback').innerfade({
	speed: 1000,
	timeout: 12000,
	type: 'random_start',
	containerheight: '120px'
	});

	var pathname = window.location.pathname;
	pathname_a = pathname.split("/");
	var l = pathname_a.length;

	$("#mainmenu li h3").each(function(){
		var h3a = jQuery(this).find('a').attr('name');
		if (h3a)
		{
			var h3a_a = h3a.split("/");

			if (h3a_a[1] == pathname_a[1]) {
				$(this).addClass('active');
			}
		}
	});

	$("#mainmenu li ul li").each(function(){
		var at = $(this).find('a').attr('href');
		var at_a = at.split("/");

		if (at == pathname) {
			$(this).find('a').parent().parent().prev().addClass('active');
			$(this).find('a').parent().addClass('active');
		} else {
			if ($(this).parent().attr('class') == 'last-nav') {

			} else {
			if (pathname_a[1] == at_a[1]) {
				if (pathname_a[2] == at_a[2]) {
					$(this).find('a').parent().parent().prev().addClass('active');
						$(this).find('a').parent().addClass('active');
				}
				if (pathname_a[2] != null) {
					if (pathname_a[2].indexOf('jongen') != -1) {
						$(this).find('a').parent().parent().prev().addClass('active');
						$(this).find('a').parent().addClass('active');
					}

					if (pathname_a[2].indexOf('meisje') != -1) {
						$(this).find('a').parent().parent().prev().addClass('active');
						$(this).find('a').parent().addClass('active');
					}

					if (pathname_a[2].indexOf('tweeling') != -1) {
						$(this).find('a').parent().parent().prev().addClass('active');
						$(this).find('a').parent().addClass('active');
					}

					if (pathname_a[2].indexOf('adoptie') != -1) {
						$(this).find('a').parent().parent().prev().addClass('active');
						$(this).find('a').parent().addClass('active');
					}


					if (pathname_a[2].indexOf('boefjespost') != -1) {
						$(this).find('a').parent().parent().prev().addClass('active');
						$(this).find('a').parent().addClass('active');
					}

					if (pathname_a[2].indexOf('babystuff') != -1) {
						$(this).find('a').parent().parent().prev().addClass('active');
						$(this).find('a').parent().addClass('active');
					}

					if (pathname_a[2].indexOf('creagaat') != -1) {
						$(this).find('a').parent().parent().prev().addClass('active');
						$(this).find('a').parent().addClass('active');
					}

					if (pathname_a[2].indexOf('dysyn') != -1) {
						$(this).find('a').parent().parent().prev().addClass('active');
						$(this).find('a').parent().addClass('active');
					}

					if (pathname_a[2].indexOf('lief-kindje') != -1) {
						$(this).find('a').parent().parent().prev().addClass('active');
						$(this).find('a').parent().addClass('active');
					}

					if (pathname_a[2].indexOf('marsanda') != -1) {
						$(this).find('a').parent().parent().prev().addClass('active');
						$(this).find('a').parent().addClass('active');
					}

					if (pathname_a[2].indexOf('xantifee') != -1) {
						$(this).find('a').parent().parent().prev().addClass('active');
						$(this).find('a').parent().addClass('active');
					}
				}
			}
			}
		}
	});

	$("#mainmenu li").find("h3.active").parent().find('ul').show();
	//$("#mainmenu li:eq(0) ul").show();
	$("#mainmenu li h3").addClass("close");
	$("#mainmenu li h3.active").addClass("open");
	if ($("#mainmenu .close").length < 2){
		$("#mainmenu li").find('.close').addClass('open').removeClass('close');
		$("#mainmenu li ul").show();
	}

	$("#mainmenu li .close").click(function(){
		var inner=$(this).parents("li").find("ul");
		if($(this).hasClass("open")){
			$(this).removeClass("open");
			inner.slideUp(300);

		}else{
			$("#mainmenu li ul").not(inner).hide();
			$("#mainmenu li .close").removeClass("open");
			$(this).addClass("open");
			inner.slideDown(300);
		}
	});


	$('#menu li').each(function() {
		var hre = $(this).find('a').attr('href');
		if (hre == pathname) {
			$(this).find('a').addClass('active');
		}
	});

	var src = $('#body_main_category #pagecontent .main-img img').attr('src');
	if (src != null) {
		$('#body_main_category #pagecontent h1').before('<img src="' + src + '" alt="" id="main-img">');
	}
	$('#body_design h1').addClass('title_pro');
	$( "<a/>", {
		"class": "previousPage",
		text: "Terug naar overzicht",
		click: function() {
			parent.history.back();
			return false;
		}
	})
	.insertAfter( "h1.title_pro" );
	jQuery('#categories .cate_more').click(function(){
		if (!jQuery(this).hasClass('selected')){
			jQuery(this).next('#mainmenu').slideDown();
			jQuery(this).addClass('selected');
		} else {
			jQuery(this).next('#mainmenu').slideUp();
			jQuery(this).removeClass('selected');
		}
	});

	if ($('#textblock').length > 0) {var ht = $('#textblock').html();$('#categories').append(ht);$('#textblock').remove();}
    setHome();

    //Search function
    $('a.zoek').on('click', function () {
        $(this).toggleClass('activeS');
        $(this).toggleClass('active').parent().find('.sForm').fadeToggle(function () {
            $('.search-wrap input').focus();
        });

    });
});

$(window).load(function(){
    if($('div.sliderBlock').length){
        $('div.slideWrap').cycle({
            timeout: 0,
            slides: '> a',
            prev: '#pSlide',
            next: '#nSlide',
            fx: 'carousel',
            allowWrap: true,
            speed: 500
        });
    }
});

function setHome(){
    if($('.mainCont').length) {
        $('.mainCont').append('<div class="sliderBlock"><div id="pSlide"></div><div id="nSlide"></div><div class="slideWrap"></div></div>');
        $('#card_previews a').each(function(i){
                $(this).appendTo('.slideWrap');
        })
        $('#card_previews').remove();
    } else {
        console.log('Er is geen mainCont div aanwezig');
    }
}
