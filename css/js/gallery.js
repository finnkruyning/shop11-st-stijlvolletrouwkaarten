jQuery.fn.scrollGallery = function (e) {
    var e = jQuery.extend({
        sliderHolder: ">div",
        slider: ">ul",
        slides: ">li",
        pagerLinks: "div.pager a",
        btnPrev: "a.link-prev",
        btnNext: "a.link-next",
        activeClass: "active",
        disabledClass: "disabled",
        generatePagination: "div.pg-holder",
        curNum: "em.scur-num",
        allNum: "em.sall-num",
        circleSlide: true,
        pauseClass: "gallery-paused",
        pauseButton: "none",
        pauseOnHover: true,
        autoRotation: false,
        stopAfterClick: false,
        switchTime: 5e3,
        duration: 650,
        easing: "swing",
        event: "click",
        afterInit: false,
        vertical: false,
        step: false
    }, e);
    return this.each(function () {
        function H() {
            if (S) {
                if (E) {
                    M = i.eq(C).outerHeight(true);
                    _ = Math.ceil((L - n.height()) / M) + 1;
                    D = -M * C
                } else {
                    M = n.height();
                    _ = Math.ceil(L / M);
                    D = -M * C;
                    if (D < M - L) D = M - L
                }
            } else {
                if (E) {
                    O = i.eq(C).outerWidth(true) * E;
                    _ = Math.ceil((k - n.width()) / O) + 1;
                    D = -O * C;
                    if (D < n.width() - k) D = n.width() - k
                } else {
                    O = n.width();
                    _ = Math.ceil(k / O);
                    D = -O * C;
                    if (D < O - k) D = O - k
                }
            }
        }
        function F() {
            H();
            if (C > 0) C--;
            else if (x) C = _ - 1;
            R()
        }
        function I() {
            H();
            if (C < _ - 1) C++;
            else if (x) C = 0;
            R()
        }
        function q() {
            if (u.length) u.removeClass(v).eq(C).addClass(v);
            if (!x) {
                s.removeClass(m);
                o.removeClass(m);
                if (C == 0) s.addClass(m);
                if (C == _ - 1) o.addClass(m)
            }
            if (f.length) f.text(C + 1);
            if (l.length) l.text(_)
        }
        function R() {
            H();
            if (S) r.animate({
                    marginTop: D
                }, {
                    duration: y,
                    queue: false,
                    easing: g
                });
            else r.animate({
                    marginLeft: D
                }, {
                    duration: y,
                    queue: false,
                    easing: g
                });
            q();
            z()
        }
        function U() {
            if (P) clearTimeout(P);
            d = false
        }
        function z() {
            if (!d || A) return;
            if (P) clearTimeout(P);
            P = setTimeout(I, b + y)
        }
        var t = jQuery(this);
        var n = jQuery(e.sliderHolder, t);
        var r = jQuery(e.slider, n);
        var i = jQuery(e.slides, r);
        var s = jQuery(e.btnPrev, t);
        var o = jQuery(e.btnNext, t);
        var u = jQuery(e.pagerLinks, t);
        var a = jQuery(e.generatePagination, t);
        var f = jQuery(e.curNum, t);
        var l = jQuery(e.allNum, t);
        var c = jQuery(e.pauseButton, t);
        var h = e.pauseOnHover;
        var p = e.pauseClass;
        var d = e.autoRotation;
        var v = e.activeClass;
        var m = e.disabledClass;
        var g = e.easing;
        var y = e.duration;
        var b = e.switchTime;
        var w = e.event;
        var E = e.step;
        var S = e.vertical;
        var x = e.circleSlide;
        var T = e.stopAfterClick;
        var N = e.afterInit;
        if (!i.length) return;
        var C = 0;
        var k = 0;
        var L = 0;
        var A = false;
        var O;
        var M;
        var _;
        var D;
        var P;
        i.each(function () {
            k += $(this).outerWidth(true);
            L += $(this).outerHeight(true)
        });
        if (s.length) {
            s.bind(w, function () {
                if (T) U();
                F();
                return false
            })
        }
        if (o.length) {
            o.bind(w, function () {
                if (T) U();
                I();
                return false
            })
        }
        if (a.length) {
            a.empty();
            H();
            var B = $("<ul />");
            for (var j = 0; j < _; j++) $('<li><a href="#">' + (j + 1) + "</a></li>").appendTo(B);
            B.appendTo(a);
            u = B.children()
        }
        if (u.length) {
            u.each(function (e) {
                jQuery(this).bind(w, function () {
                    if (C != e) {
                        if (T) U();
                        C = e;
                        R()
                    }
                    return false
                })
            })
        }
        if (h) {
            t.hover(function () {
                A = true;
                if (P) clearTimeout(P)
            }, function () {
                A = false;
                z()
            })
        }
        H();
        q();
        z();
        if (c.length) {
            c.click(function () {
                if (t.hasClass(p)) {
                    t.removeClass(p);
                    d = true;
                    z()
                } else {
                    t.addClass(p);
                    U()
                }
                return false
            })
        }
        if (N && typeof N === "function") N(t, i)
    })
};
jQuery.fn.fadeGallery = function (e) {
    var e = jQuery.extend({
        slideElements: "div.slideset > div",
        pagerLinks: "div.pager a",
        btnNext: "a.next",
        btnPrev: "a.prev",
        btnPlayPause: "a.play-pause",
        btnPlay: "a.play",
        btnPause: "a.pause",
        pausedClass: "paused",
        disabledClass: "disabled",
        playClass: "playing",
        activeClass: "active",
        currentNum: false,
        allNum: false,
        startSlide: null,
        noCircle: false,
        pauseOnHover: true,
        autoRotation: false,
        autoHeight: false,
        onChange: false,
        switchTime: 3e3,
        duration: 650,
        event: "click"
    }, e);
    return this.each(function () {
        function A() {
            N = C;
            if (C > 0) C--;
            else {
                if (S) return;
                else C = k - 1
            }
            _()
        }
        function O() {
            N = C;
            if (C < k - 1) C++;
            else {
                if (S) return;
                else C = 0
            }
            _()
        }
        function M() {
            if (r.length) r.removeClass(c).eq(C).addClass(c);
            if (b) b.text(C + 1);
            if (w) w.text(k);
            n.eq(N).removeClass(c);
            n.eq(C).addClass(c);
            if (S) {
                if (i.length) {
                    if (C == 0) i.addClass(h);
                    else i.removeClass(h)
                }
                if (s.length) {
                    if (C == k - 1) s.addClass(h);
                    else s.removeClass(h)
                }
            }
            if (typeof x === "function") {
                x(t, C)
            }
        }
        function _() {
            n.eq(N).animate({
                opacity: 0
            }, {
                duration: m,
                queue: false
            });
            n.eq(C).css({
                display: "block",
                opacity: 0
            }).animate({
                opacity: 1
            }, {
                duration: m,
                queue: false
            });
            if (v) n.eq(C).parent().animate({
                    height: n.eq(C).outerHeight(true)
                }, {
                    duration: m,
                    queue: false
                });
            M();
            D()
        }
        function D() {
            if (!l || T) return;
            if (L) clearTimeout(L);
            L = setTimeout(O, g + m)
        }
        var t = jQuery(this);
        var n = jQuery(e.slideElements, t);
        var r = jQuery(e.pagerLinks, t);
        var i = jQuery(e.btnPrev, t);
        var s = jQuery(e.btnNext, t);
        var o = jQuery(e.btnPlayPause, t);
        var u = jQuery(e.btnPause, t);
        var a = jQuery(e.btnPlay, t);
        var f = e.pauseOnHover;
        var l = e.autoRotation;
        var c = e.activeClass;
        var h = e.disabledClass;
        var p = e.pausedClass;
        var d = e.playClass;
        var v = e.autoHeight;
        var m = e.duration;
        var g = e.switchTime;
        var y = e.event;
        var b = e.currentNum ? jQuery(e.currentNum, t) : false;
        var w = e.allNum ? jQuery(e.allNum, t) : false;
        var E = e.startSlide;
        var S = e.noCircle;
        var x = e.onChange;
        var T = false;
        var N = 0;
        var C = 0;
        var k = n.length;
        var L;
        if (k < 2) return;
        N = n.index(n.filter("." + c));
        if (N < 0) N = C = 0;
        else C = N; if (E != null) {
            if (E == "random") N = C = Math.floor(Math.random() * k);
            else N = C = parseInt(E)
        }
        n.css({
            display: "block",
            opacity: 0
        }).eq(C).css({
            opacity: 1
        });
        if (l) t.removeClass(p).addClass(d);
        else t.removeClass(d).addClass(p); if (i.length) {
            i.bind(y, function () {
                A();
                return false
            })
        }
        if (s.length) {
            s.bind(y, function () {
                O();
                return false
            })
        }
        if (r.length) {
            r.each(function (e) {
                jQuery(this).bind(y, function () {
                    if (C != e) {
                        N = C;
                        C = e;
                        _()
                    }
                    return false
                })
            })
        }
        if (o.length) {
            o.bind(y, function () {
                if (t.hasClass(p)) {
                    t.removeClass(p).addClass(d);
                    l = true;
                    D()
                } else {
                    l = false;
                    if (L) clearTimeout(L);
                    t.removeClass(d).addClass(p)
                }
                return false
            })
        }
        if (a.length) {
            a.bind(y, function () {
                t.removeClass(p).addClass(d);
                l = true;
                D();
                return false
            })
        }
        if (u.length) {
            u.bind(y, function () {
                l = false;
                if (L) clearTimeout(L);
                t.removeClass(d).addClass(p);
                return false
            })
        }
        if (f) {
            t.hover(function () {
                T = true;
                if (L) clearTimeout(L)
            }, function () {
                T = false;
                D()
            })
        }
        M();
        D()
    })
};

(function () {
    jQuery.fn.fixedSlideBlock = function (n) {
        var n = jQuery.extend({
            slideBlock: "#sidebar",
            alwaysStick: true,
            animDelay: 100,
            animSpeed: 250,
            extraHeight: 0,
            positionType: "auto"
        }, n);
        return this.each(function () {
            function c() {
                var e = s.attr("style");
                s.css({
                    position: "",
                    left: "",
                    top: ""
                });
                a = {
                    winHeight: r.height(),
                    scrollTop: r.scrollTop(),
                    scrollLeft: r.scrollLeft(),
                    holderOffset: i.offset(),
                    holderHeight: i.height(),
                    blockPosition: s.position(),
                    blockOffset: s.offset(),
                    blockHeight: s.outerHeight(true)
                };
                s.attr("style", e);
                if (l !== a.holderHeight) {
                    l = a.holderHeight;
                    f = h()
                }
            }
            function h() {
                var e = s.attr("style");
                var t = a.holderHeight;
                s.css({
                    position: "absolute"
                });
                var n = i.height();
                if (n < t) {
                    s.css({
                        position: "",
                        top: "",
                        left: ""
                    });
                    return true
                } else {
                    s.attr("style", e)
                }
            }
            function p() {
                if (a.scrollTop > a.blockOffset.top - n.extraHeight) {
                    if (a.scrollTop + n.extraHeight - a.holderOffset.top + a.blockHeight > a.holderHeight) {
                        s.css({
                            position: "absolute",
                            left: a.blockPosition.left,
                            top: a.blockPosition.top + a.holderHeight - a.blockHeight - (a.blockOffset.top - a.holderOffset.top)
                        })
                    } else {
                        s.css({
                            position: "fixed",
                            left: a.blockOffset.left - a.scrollLeft,
                            top: n.extraHeight
                        })
                    }
                } else {
                    s.css({
                        position: "",
                        top: "",
                        left: ""
                    })
                }
            }
            function d(e) {
                var t = a.blockPosition.top;
                if (a.scrollTop > a.blockOffset.top - n.extraHeight) {
                    if (a.scrollTop + n.extraHeight - a.holderOffset.top + a.blockHeight > a.holderHeight) {
                        t = a.blockPosition.top + a.holderHeight - a.blockHeight - (a.blockOffset.top - a.holderOffset.top)
                    } else {
                        t = a.blockPosition.top + a.scrollTop - a.blockOffset.top + n.extraHeight
                    }
                }
                s.stop().css({
                    position: "absolute",
                    left: a.blockPosition.left
                });
                if (e === true) {
                    s.css({
                        top: t
                    })
                } else {
                    s.animate({
                        top: t
                    }, {
                        duration: n.animSpeed
                    })
                }
            }
            function v(e) {
                var t = e === true;
                c();
                if (f) {
                    return
                }
                if (!n.alwaysStick && a.winHeight < a.blockHeight) {
                    s.css({
                        position: "",
                        top: "",
                        left: ""
                    });
                    return
                }
                if (o === "fixed") {
                    p()
                } else {
                    if (t) {
                        d(t)
                    }
                    clearTimeout(u);
                    u = setTimeout(d, n.animDelay)
                }
            }
            var r = jQuery(window);
            var i = jQuery(this);
            var s = i.find(n.slideBlock);
            var o = n.positionType,
                u, a, f, l;
            if (o === "auto") {
                o = !t || e ? "absolute" : "fixed"
            }
            setTimeout(function () {
                v(true);
                r.bind("resize scroll", v)
            }, 10)
        })
    };
    var e = function () {
        try {
            return "ontouchstart" in window || window.DocumentTouch && document instanceof DocumentTouch
        } catch (e) {
            return false
        }
    }();
    var t = function () {
        var e = false,
            t = document.createElement("div"),
            n = document.createElement("div");
        t.style.cssText = "position:absolute;top:9px;left:9px;width:100px;height:100px;background:red;";
        n.style.cssText = "position:fixed;top:7px;width:1px;height:1px;";
        t.appendChild(n);
        document.documentElement.insertBefore(t, document.documentElement.childNodes[0]);
        e = n.offsetTop == 7 || n.offsetTop == -2 || !n.offsetWidth && typeof document.documentElement.style.maxHeight !== "undefined";
        document.documentElement.removeChild(t);
        n = t = null;
        return e
    }()
})();

function initGalleries() {
    $("div.style-2").each(function () {
        $(this).find(".carousel").scrollGallery({
            sliderHolder: ">div",
            slider: ">ul",
            slides: ">li",
            btnPrev: "a.link-prev",
            btnNext: "a.link-next",
            circleSlide: true,
            autoRotation: false,
            stopAfterClick: false,
            switchTime: 5e3,
            duration: 650,
            easing: "swing",
            event: "click",
            afterInit: false,
            vertical: false,
            step: 1
        });
        $(this).fadeGallery({
            slideElements: "ul.large-img > li",
            pagerLinks: ".carousel div.holder li",
            activeClass: "active",
            pauseOnHover: true,
            autoRotation: false,
            autoHeight: false,
            switchTime: 3e3,
            duration: 500,
            event: "click"
        })
    });
}

$(function(){
	initGalleries();	   
})