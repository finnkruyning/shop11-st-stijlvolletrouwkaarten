$(function () {

    // $(".qry").keypress(function (e) {
    //     if ((e.which && e.which == 13) || (e.keyCode && e.keyCode == 13)) {
    //         var input = $('.qry').val();
    //         console.log(input)
    //         window.location.href = '/zoeken?qry=' + input;
    //         return;
    //     } else {
    //         return true;
    //     }
    // });
    
    // $('#tag_dialog').prepend('<button class="alles_uitvinken_tags">Alles uitvinken</button>');
    // $('body').on('click', '.alles_uitvinken_tags', function(e){
    //     e.preventDefault();
    //     $('#save_overview_tags_form').find('input[type=checkbox]:checked').not("[disabled]").removeAttr('checked');
    // });

    var $header = $('#header');

    var $headerMenu = $header.find('.topbar-menu-right');
    $headerMenu.find('#navbar-basket').html($("#headerbasket #basket_count").html());
    if (jQuery("#headerlogout").length > 0) {
        logout = "<a href='/logout' title='Uitloggen' class='logout'><span>" + jQuery("#headerlogout span").html() + "</span></a>";
    }
    if (!$("#headerlogin").length) {
        $headerMenu.find('#navbar-login').remove();
    }

    if (!$("#headerlogout").length) {
        $headerMenu.find('#navbar-logout').remove();
    }

    // append visual content
    // if ($header.find('#visual-content').length === 0) {
    //     $header.append('<div id="visual-content"></div>');
    // }

    // stuff in mid
    // var $mid = $('#mid');

    // var $sidebar = $mid.find('#sidebar');

    // var $pagecontent = $mid.find('#pagecontent');
    // $pagecontent.find('.visual').appendTo($header.find('#visual-content')); // set visual to visual content in header

    // check type of page should be added to first dive
    // has id fluid-template
    // has id sidebar-template
    // if ($pagecontent.find('#template-fluid').length) {
    //     $pagecontent.removeClass("sm-9 md-9").addClass("sm-12 md-12");
    //     $sidebar.remove();
    // } else if ($pagecontent.find('#template-sidebar').length) {
    //     $pagecontent.removeClass("sm-12 md-9").addClass("sm-9 md-9");
    // } else {
    //     $pagecontent.removeClass("sm-12 md-12").addClass("md-9");
    // }

    // set the menu's in sidebar. plaats in een pagina een <div data-sidebar="menu1|menu2|menut3" en in het menu de attr die je wilt laten zien <div id="menu1">
    // var $dataSidebar = $pagecontent.find('[data-sidebar]');

    // if ($dataSidebar.length) {
    //     $dataSidebar.data('sidebar').split("|").map(function (response) {
    //         if ($sidebar.find('#' + response).length) {
    //             $sidebar.find('#' + response).show();
    //         }
    //     });

    // } else {
    //     $sidebar.find('.hide').show();
    // }

    // $(".accordion-body ul li a[href='" + window.location.pathname + "']").addClass('active').parents('.accordion-body').addClass('show').prev('.accordion-button').addClass('active');

    // //Add class cardrail
    // $('#card_previews_horizontal').parent().addClass('cardrail');
    // $('#footer p.rich-snippets').prependTo('.extraBox');

    // var $filterCard = $('#filter_cards');
    // var $titleFilter = 'Filters';
    // if ($filterCard.length) {
    //     $filterCard.before('<h2>' + $titleFilter + '</h2>');
    // }

});

$(function () {

    $('#crumbs').remove().insertBefore($('#categories')).show();


    var e = $(".tag-group-ul");
    // e.each(function () {
    //     var e = $(this),
    //         n = e.find("h3"),
    //         i = e.find("ul");
    //     i.addClass("show"), $('<i class="icon"></i>').appendTo(n), i.hasClass("show") ? (e.find("h3").addClass("active"), n.find(".icon").addClass("icon-chevron-down")) : (e.find("h3").removeClass("active"), n.find(".icon").addClass("icon-chevron-right"))
    // }), e.find("h3").on("click", function (n) {
    //     n.preventDefault();
    //     var i = $(this),
    //         a = i.closest(e).find("ul"),
    //         t = i.find(".icon");
    //     a.toggleClass("show"), a.is(":visible") ? (i.addClass("active"), t.removeClass("icon-chevron-right").addClass("icon-chevron-down")) : (i.removeClass("active"), t.removeClass("icon-chevron-down").addClass("icon-chevron-right"))
    // })


    // var filterMob = function () {
    //     //Tablet filter functionaliteiten
    //     if ($(window).width() <= 820) {
    //         if ($('.filCTA').length && $('.filCTA').is(':visible')) {
    //             $('#sidebar nav').prependTo('body').addClass('mobFilter');

    //             $('a.filCTA').on('click', function (e) {
    //                 e.preventDefault();
    //                 $('body').addClass('hide-body-scroll');
    //                 $('div.filCTAwrp').toggleClass('active');
    //                 $('.mobFilter').toggleClass('active');
    //                 $('.filRes em').html($('#card_previews>div').length);
    //                 $('body').toggleClass('noflow');
    //             });

    //             $('li.tag-li').on('click', function () {
    //                 setTimeout(function () {
    //                     $('.filRes em').html($('#card_previews>div').length);
    //                 }, 500);
    //             });

    //             $('.filRes').on('click', function () {
    //                 $('body').removeClass('hide-body-scroll');
    //                 $('div.filCTAwrp').toggleClass('active');
    //                 $('.mobFilter').toggleClass('active');
    //                 $('body').toggleClass('noflow');
    //             });
    //         }
    //     }
    // }

    // $(window).resize(function () {
    //     filterMob()
    // })

    // filterMob();


    $('#sidebar .widget_content ul.list > li:first-child > a').each(function(){
        $(this).parent('li').css({'maxHeight': ($(this).parent('li').children('ul').height()+55) + 'px'});
        $(this).parent('li').addClass('active');
    });

    $('#sidebar .widget_content').on('click', '.list.list-vertical > li > a', function(e){
        e.preventDefault();

        $(this).parent('li').siblings('li').each(function(){
            $(this).removeClass('active');
            $(this).css({'maxHeight': '55px'});
        });


        if($(this).parent('li').hasClass('active')){
            $(this).parent('li').css({'maxHeight': '55px'});
        } else {
            $(this).parent('li').css({'maxHeight': ($(this).parent('li').children('ul').height()+55) + 'px'});

        }
        $(this).parent('li').toggleClass('active');
    });

    $('#sidebar #filter_menu').on('click', 'h3', function(e){
        e.preventDefault();

        // $(this).parent('li').siblings('li').each(function(){
        //     $(this).removeClass('active');
        //     $(this).css({'maxHeight': '55px'});
        // });
        $(this).toggleClass('close');


        if($(this).hasClass('close')){
            $(this).parent('li').css({'maxHeight': '55px'});
        } else {
            $(this).parent('li').css({'maxHeight': ($(this).parent('li').children('ul').height()+55) + 'px'});

        }
    });






});






// $(document).ready(function () {
//     iconheader();
// });

// $(window).resize(function () {
//     iconheader();
// })

