function shuffle(e) {
    for (var n, i, a = e.length; a; n = parseInt(Math.random() * a), i = e[--a], e[a] = e[n], e[n] = i);
    return e
}
$(function() {
    var e = $(".accordion"),
        n = window.location.pathname;
    $('.accordion-body .list-collapse li a[href="' + n + '"]').addClass("active"), e.each(function() {
        var e = $(this),
            n = e.find(".accordion-button"),
            i = e.find(".accordion-body");
        $('<i class="icon"></i>').appendTo(n), i.hasClass("show") ? (n.addClass("active"), n.find(".icon").addClass("icon-chevron-down")) : (n.removeClass("active"), n.find(".icon").addClass("icon-chevron-right"))
    }), e.find(".accordion-button").on("click", function(n) {
        n.preventDefault();
        var i = $(this),
            a = i.closest(e).find(".accordion-body"),
            t = i.find(".icon");
        a.toggleClass("show"), a.is(":visible") ? (i.addClass("active"), t.removeClass("icon-chevron-right").addClass("icon-chevron-down")) : (i.removeClass("active"), t.removeClass("icon-chevron-down").addClass("icon-chevron-right"))
    })
}), $(function() {
    var e = $(".carousel");
    e.each(function() {
        var e = $(this),
            n = e.find(".carousel-middle img");
        e.find(".carousel-left").hide(), e.find(".carousel-right").hide(), n.length > 1 && (e.find(".carousel-left").show(), e.find(".carousel-right").show(), e.find(".carousel-middle").cycle())
    }), e.find(".carousel-left a").click(function(n) {
        n.preventDefault();
        var i = $(this),
            a = i.closest(e).find(".carousel-middle");
        a.cycle("next")
    }), e.find(".carousel-right a").click(function(n) {
        n.preventDefault();
        var i = $(this),
            a = i.closest(e).find(".carousel-middle");
        a.cycle("prev")
    })
}), $(function() {
    var e = $("#header"),
        n = e.find(".topbar-menu-right"),
        i = $("#headerbasket #basket_count").text().replace("items", "");
    n.find("#navbar-basket").html(i), jQuery("#headerlogout").length > 0 && (logout = "<a href='/logout' title='Uitloggen' class='logout'><span>" + jQuery("#headerlogout span").html() + "</span></a>"), $("#headerlogin").length || n.find("#navbar-login").remove(), $("#headerlogout").length || n.find("#navbar-logout").remove(), 0 === e.find("#visual-content").length && e.append('<div id="visual-content"></div>');
    var a = $("#mid"),
        t = a.find("#sidebar"),
        o = a.find("#pagecontent");
    o.find(".visual").appendTo(e.find("#visual-content")), o.find("#template-fluid").length ? (o.removeClass("md-9").addClass("md-12"), t.remove()) : o.find("#template-sidebar").length && o.removeClass("md-12").addClass("md-9"), $("#about-us").length && $("#footer").before($("#about-us")), $("#body_design").length && $(".box-body").find(".proefDrukPrijs").text("Bestel eenvoudig een proefdruk voor â‚¬ 0.99"), $("#body_404error #pagecontent #crumbs").next("h1").hide(), $("#body_404error #pagecontent #crumbs").hide(), $("#sidebar").append($("#sidebar-footer")), $('.navbar .middlebar ul.middlebar-menu-right > li > a[href="' + window.location.pathname + '"]').addClass("active"), $(".banner-cat").length && $("#pagecontent h1").before($(".banner-cat"));
    var s = window.location.href;
    /trouwkaarten/.test(s) ? ($("body").addClass("trouwkaarten-page"), $("#menu-trouwkaarten").find(".accordion-button").addClass("active").next().addClass("show"), $("#menu-trouwkaarten").find(".icon-chevron-right").removeClass("icon-chevron-right").addClass("icon-chevron-down")) : /save-the-date-kaarten/.test(s) ? /kerst-save-the-date-kaarten/.test(s) ? ($("body").addClass("kerstkaarten-page"), $("#menu-kerstkaarten").find(".accordion-button").addClass("active").next().addClass("show"), $("#menu-kerstkaarten").find(".icon-chevron-right").removeClass("icon-chevron-right").addClass("icon-chevron-down")) : ($("body").addClass("save-the-date-page"), $("#menu-save-the-date").find(".accordion-button").addClass("active").next().addClass("show"), $("#menu-save-the-date").find(".icon-chevron-right").removeClass("icon-chevron-right").addClass("icon-chevron-down")) : /jubileumkaarten/.test(s) ? ($("body").addClass("jubileumkaarten-page"), $("#menu-jubileumkaarten").find(".accordion-button").addClass("active").next().addClass("show"), $("#menu-jubileumkaarten").find(".icon-chevron-right").removeClass("icon-chevron-right").addClass("icon-chevron-down")) : /uitnodiging-verjaardag/.test(s) ? ($("body").addClass("uitnodiging-verjaardag-page"), $("#menu-uitnodiging-verjaardag").find(".accordion-button").addClass("active").next().addClass("show"), $("#menu-uitnodiging-verjaardag").find(".icon-chevron-right").removeClass("icon-chevron-right").addClass("icon-chevron-down")) : /uitnodiging-feest/.test(s) ? ($("body").addClass("uitnodiging-feest-page"), $("#menu-uitnodiging-feest").find(".accordion-button").addClass("active").next().addClass("show"), $("#menu-uitnodiging-feest").find(".icon-chevron-right").removeClass("icon-chevron-right").addClass("icon-chevron-down")) : /uitnodiging-maken/.test(s) ? ($("body").addClass("uitnodiging-maken-page"), $("#menu-uitnodiging-maken").find(".accordion-button").addClass("active").next().addClass("show"), $("#menu-uitnodiging-maken").find(".icon-chevron-right").removeClass("icon-chevron-right").addClass("icon-chevron-down")) : /fotokaarten/.test(s) ? ($("body").addClass("fotokaarten-page"), $("#menu-fotokaarten").find(".accordion-button").addClass("active").next().addClass("show"), $("#menu-fotokaarten").find(".icon-chevron-right").removeClass("icon-chevron-right").addClass("icon-chevron-down")) : /kerstkaarten/.test(s) ? ($("body").addClass("kerstkaarten-page"), $("#menu-kerstkaarten").find(".accordion-button").addClass("active").next().addClass("show"), $("#menu-kerstkaarten").find(".icon-chevron-right").removeClass("icon-chevron-right").addClass("icon-chevron-down")) : /kerst/.test(s) ? ($("body").addClass("kerstkaarten-page"), $("#menu-kerstkaarten").find(".accordion-button").addClass("active").next().addClass("show"), $("#menu-kerstkaarten").find(".icon-chevron-right").removeClass("icon-chevron-right").addClass("icon-chevron-down")) : $("body").addClass("other-page"), $("#body_home #categories").remove(), $("#categories").before('<div class="line line-top" style="margin-bottom:13px;margin-top:0;"></div>'), $("#categories").before($("#crumbs")), $(".trouwkaarten-page #header .logo img").attr("src", "/img/logo-2.png"), $(".save-the-date-page #header .logo img").attr("src", "/img/logo-2.png"), $(".kerstkaarten-page #header .logo img").attr("src", "/img/logo-3.png"), jQuery(window).width() <= 820 && $(".filCTA").length && $(".filCTA").is(":visible") && ($("#sidebar nav").prependTo("body").addClass("mobFilter"), $("a.filCTA").on("click", function(e) {
        e.preventDefault(), $("div.filCTAwrp").toggleClass("active"), $(".mobFilter").toggleClass("active"), $(".filRes em").html($("#card_previews>div").length), $("body").toggleClass("noflow")
    }), $("li.tag-li").on("click", function() {
        setTimeout(function() {
            $(".filRes em").html($("#card_previews>div").length)
        }, 500)
    }), $(".filRes").on("click", function() {
        $("div.filCTAwrp").toggleClass("active"), $(".mobFilter").toggleClass("active"), $("body").toggleClass("noflow")
    })), $.ajax({
        type: "GET",
        url: "/css/xml/reviews-fuif.xml",
        dataType: "xml",
        success: function(e) {
            var n = shuffle($("review", e)),
                i = 1;
            n.filter(":lt(" + i + ")").each(function(e, n) {
                var i = $(n).find("description").text();
                $(n).find("url").text();
                $("#footer .feedback").append('<p>"' + i + '"</p>')
            })
        },
        error: function() {
            $("#footer .feedback").html("An error occurred while processing XML file.")
        }
    })
}), $(function() {
    var e = $(".tag-group-ul");
    e.each(function() {
        var e = $(this),
            n = e.find("h3"),
            i = e.find("ul");
        i.addClass("show"), $('<i class="icon"></i>').appendTo(n), i.hasClass("show") ? (e.find("h3").addClass("active"), n.find(".icon").addClass("icon-chevron-down")) : (e.find("h3").removeClass("active"), n.find(".icon").addClass("icon-chevron-right"))
    }), e.find("h3").on("click", function(n) {
        n.preventDefault();
        var i = $(this),
            a = i.closest(e).find("ul"),
            t = i.find(".icon");
        a.toggleClass("show"), a.is(":visible") ? (i.addClass("active"), t.removeClass("icon-chevron-right").addClass("icon-chevron-down")) : (i.removeClass("active"), t.removeClass("icon-chevron-down").addClass("icon-chevron-right"))
    })
}), $(function() {
    var e = $(".list"),
        n = 500;
    e.each(function() {
        var e = $(this);
        e.hasClass("list-collapse") && (e.find(">li:gt(" + (n - 1) + ")").hide(), e.append('<li class="list-toggle"><a class="list-toggle-text" href="#">Meer resultaten <i class="icon icon-x1 icon-chevron-right">&nbsp;</i></a></li>'), e.find(">li:not(:last-child)").length > n ? e.find(".list-toggle").show() : e.find(".list-toggle").hide())
    }), e.find(".list-toggle").click(function(i) {
        i.preventDefault();
        var a = $(this),
            t = a.closest(e).find(">li:gt(" + (n - 1) + ")");
        a.hasClass("list-toggle-show") ? (a.find(".list-toggle-text").html('Meer resultaten <i class="icon icon-x1 icon-chevron-right">&nbsp;</i>'), a.removeClass("list-toggle-show"), t.hide().last().show()) : (a.find(".list-toggle-text").html('Minder resultaten <i class="icon icon-x1 icon-chevron-down">&nbsp;</i>'), a.addClass("list-toggle-show"), t.show().last().show())
    })
}), $(function() {
    var e = $(".navbar");
    e.find(".navbar-search-field").hide(), e.find(".open-navbar-search").on("click", function(n) {
        n.preventDefault(), n.stopImmediatePropagation();
        var i = $(this),
            a = i.closest(e).find(".navbar-search-field");
        a.toggle(), a.on("click", function(e) {
            e.preventDefault(), e.stopImmediatePropagation()
        })
    }), e.find(".navbar-hamburger").on("click", function(n) {
        n.preventDefault();
        var i = $(this),
            a = i.closest(e).find(".collapse");
        a.toggle(), a.is(":visible") ? $("html, body").css("overflowY", "hidden") : $("html, body").css("overflowY", "auto")
    }), e.find(".dropdown > a").on("click", function(e) {
        e.preventDefault();
        var n = $(this),
            i = n.closest(".dropdown").find(".dropdown-content");
        i.toggle()
    }), e.find(".dropdown > a").on("mouseenter", function(e) {
        e.preventDefault();
        var n = $(this),
            i = n.closest(".dropdown").find(".dropdown-content");
        i.show()
    }), $(document).click(function() {
        e.find(".navbar-search-field").hide()
    })
}), $(document).ready(function() {
    function e(e) {
        e.wrap("<div class='table-wrapper' />");
        var n = e.clone();
        n.find("td:not(:first-child), th:not(:first-child)").css("display", "none"), n.removeClass("responsive"), e.closest(".table-wrapper").append(n), n.wrap("<div class='pinned' />"), e.wrap("<div class='scrollable' />"), i(e, n)
    }

    function n(e) {
        e.closest(".table-wrapper").find(".pinned").remove(), e.unwrap(), e.unwrap()
    }

    function i(e, n) {
        var i = e.find("tr"),
            a = n.find("tr"),
            t = [];
        i.each(function(e) {
            var n = $(this),
                i = n.find("th, td");
            i.each(function() {
                var n = $(this).outerHeight(!0);
                t[e] = t[e] || 0, n > t[e] && (t[e] = n)
            })
        }), a.each(function(e) {
            $(this).height(t[e])
        })
    }
    var a = !1,
        t = function() {
            return $(window).width() < 767 && !a ? (a = !0, $("table.responsive").each(function(n, i) {
                e($(i))
            }), !0) : (a && $(window).width() > 767 && (a = !1, $("table.responsive").each(function(e, i) {
                n($(i))
            })), void $("#card_previews_horizontal").parent().addClass("cardrail"))
        };
    $(window).load(t), $(window).on("redraw", function() {
        a = !1, t()
    }), $(window).on("resize", t)
}), $(function() {
    var e = $(".thumbnail-scroller");
    e.each(function() {
        var e = $(this);
        e.find(".thumbnail-scroller-middle").cycle({
            fx: "carousel",
            slides: "> a",
            speed: 600,
            swipe: !0,
            carouselFluid: !0,
            sync: !1
        })
    }), e.find(".thumbnail-scroller-right a").click(function(n) {
        n.preventDefault();
        var i = $(this),
            a = i.closest(e).find(".thumbnail-scroller-middle");
        a.cycle("next")
    }), e.find(".thumbnail-scroller-left a").click(function(n) {
        n.preventDefault();
        var i = $(this),
            a = i.closest(e).find(".thumbnail-scroller-middle");
        a.cycle("prev")
    })
}), $(function() {
    var e = $(".thumbnail-slider");
    e.each(function() {
        var e = $(this),
            n = e.find(".thumbnail-slider-middle img");
        e.find(".thumbnail-slider-right").hide(), e.find(".thumbnail-slider-left").hide(), n.length > 1 && (e.find(".thumbnail-slider-right").show(), e.find(".thumbnail-slider-left").show(), e.find(".thumbnail-slider-middle").cycle())
    }), e.find(".thumbnail-slider-right a").click(function(n) {
        n.preventDefault();
        var i = $(this),
            a = i.closest(e).find(".thumbnail-slider-middle");
        a.cycle("next")
    }), e.find(".thumbnail-slider-left a").click(function(n) {
        n.preventDefault();
        var i = $(this),
            a = i.closest(e).find(".thumbnail-slider-middle");
        a.cycle("prev")
    })
});