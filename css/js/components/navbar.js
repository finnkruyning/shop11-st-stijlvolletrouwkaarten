$(function () {
    var $navbar = $('.navbar');

    $navbar.find('.navbar-search-field').hide();

    $navbar.find('.open-navbar-search').on('click', function (e) {
        e.preventDefault();
        e.stopPropagation();
        var _self = $(this);
        var target = _self.closest($navbar).find('.navbar-search-field');

        target.toggle();

        target.on('click', function (e) {
            e.preventDefault();
            e.stopImmediatePropagation();
        });

    });

    $navbar.find('.navbar-hamburger:not(.js-open-middlebar-menu-left-mob)').on('click', function (e) {
        console.log(123);
        e.preventDefault();
        var _self = $(this);
        var target = _self.closest($navbar).find('.collapse');

        target.toggle();

        if (target.is(":visible")) {
            $('html, body').css('overflowY', 'hidden');
        } else {
            $('html, body').css('overflowY', 'auto');
        }
    });

    $navbar.find('.dropdown > a').on('click', function (e) {
        console.log(123);
        e.preventDefault();
        var _self = $(this);
        var target = _self.closest('.dropdown').find('.dropdown-content');
        target.toggle();
    });

    $navbar.find('.dropdown > a').on('mouseenter', function (e) {
        console.log(121233);
        e.preventDefault();
        var _self = $(this);
        var target = _self.closest('.dropdown').find('.dropdown-content');
        target.show();
    });
    $(document).click(function () {
        $navbar.find('.navbar-search-field').hide();

    });

    $('.topbar .topbar-menu-right .dropdown > a').click(function(e){
        if (jQuery(window).width() <= 992) {
            e.preventDefault();
            if ($(this).hasClass('opened')) {
                $(this).next().slideUp('fast');
                $(this).removeClass('opened');
                $(this).parent('.dropdown').removeClass('parent-open');
            } else {
                $(this).next().slideDown('fast');
                $(this).next().css('visibility','visible');
                $(this).addClass('opened');
                $(this).parent('.dropdown').addClass('parent-open');
            }
        }
    });
});