$(function () {
    var $carousel = $('.carousel');

    $carousel.each(function () {
        var _self = $(this);
        var middleCenter = _self.find('.carousel-middle img');
        _self.find('.carousel-left').hide();
        _self.find('.carousel-right').hide();

        if (middleCenter.length > 1) {
            _self.find('.carousel-left').show();
            _self.find('.carousel-right').show();
            _self.find('.carousel-middle').cycle();
        }

    });

    $carousel.find('.carousel-left a').click(function (e) {
        e.preventDefault();
        var _self = $(this);
        var target = _self.closest($carousel).find('.carousel-middle');
        target.cycle('next');
    });

    $carousel.find('.carousel-right a').click(function (e) {
        e.preventDefault();
        var _self = $(this);
        var target = _self.closest($carousel).find('.carousel-middle');
        target.cycle('prev');
    });

});

