$(document).ready(function(){
    $('.inspiBlok').parent().addClass('mainBlok');
    $('.inspiBlok').each(function(){
        var blcTitle = $(this).data('titel');
        $(this).addClass($(this).data('titel').replace(/ /g,''));
        var blcCopy = $(this).data('copy');
        var blcLink = $(this).data('copylink');
        $(this).find('a').each(function(){
            var thisLink = $(this).attr('href');
            $(this).attr('title', blcTitle).wrap('<div class="inspiTem" />');
            $(this).parent().prepend('<div class="hovShow"><a title="'+blcTitle+'" rel="gal[gal]" href="'+$(this).find('img').attr('src')+'" class="vergroot"></a><a href="'+thisLink+'" class="linknaar" title="Naar bijbehorende pagina"></a></div>');
            var imgWidt = $(this).find('img').width();
            var imgWidt = imgWidt/2;
            $(this).find('img').css({marginLeft:'-'+imgWidt+'px'});
        });
        $(this).prepend('<h2>'+blcTitle+'<em><a target="_blank" href="'+blcLink+'">'+blcCopy+'</a></em></h2>');
        $(this).append('<div style="clear:both;"></div>');
    });
    //Init prettyphoto
    $("a[rel^='gal']").prettyPhoto({
        show_title: true,
        overlay_gallery: true
    });

    $('a.vergroot').on('click', function(){
        var thislink = 'http://stijlvolletrouwkaarten.nl'+$(this).next('a').attr('href');
        var thisImg = 'http://stijlvolletrouwkaarten.nl'+$(this).parent().parent().find('img').attr('src');
        $('div.pp_social').append('&nbsp;<a style="vertical-align:top" href="'+thislink+'">Bekijk bijbehorende kaart(en) ></a>');
        $('div.pp_social').prepend('<a target="_blank" href="//nl.pinterest.com/pin/create/button/?url='+thislink+'&media='+thisImg+'&description=Dit%20moet%20je%20zien!" data-pin-do="buttonPin" data-pin-config="beside"><img src="//assets.pinterest.com/images/pidgets/pinit_fg_en_rect_gray_20.png" /></a>');
    });

    if(window.location.hash) {
            var posHash = window.location.href.split('#')[1].replace(/ /g,'');
            $('.'+posHash+'').prependTo('div.mainBlok');
    }
});
